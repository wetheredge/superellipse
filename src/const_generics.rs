#[derive(Debug, Copy, Clone)]
pub struct Superellipse<const N: f64, const SIZE_X: f64, const SIZE_Y: f64>;

impl<const N: f64, const SIZE_X: f64, const SIZE_Y: f64> Superellipse<N, SIZE_X, SIZE_Y> {
    pub fn new() -> Self {
        Self
    }

    pub fn get_y(self, x: f64) -> f64 {
        SIZE_Y * (1. - (x / SIZE_X).powf(N)).powf(1. / N)
    }

    pub fn get_x(self, y: f64) -> f64 {
        SIZE_X * (1. - (y / SIZE_Y).powf(N)).powf(1. / N)
    }
}

pub type Squircle<const SIZE: f64> = Superellipse<4., SIZE, SIZE>;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn squircle() {
        let squircle = Squircle::<2.>::new();

        assert_eq!(squircle.get_y(0.), 2.);
        assert_eq!(squircle.get_y(0.2), 1.9999499981248907);
        assert_eq!(squircle.get_y(0.4), 1.9991995195515067);
        assert_eq!(squircle.get_y(0.6), 1.9959376396729704);
        assert_eq!(squircle.get_y(0.8), 1.9870752520629362);
        assert_eq!(squircle.get_y(1.), 1.9679896712654303);
        assert_eq!(squircle.get_y(1.2), 1.9317891222131345);
        assert_eq!(squircle.get_y(1.4), 1.867321539411893);
        assert_eq!(squircle.get_y(1.6), 1.7531399356491104);
        assert_eq!(squircle.get_y(1.8), 1.5315741920727575);
        assert_eq!(squircle.get_y(2.), 0.);
    }
}

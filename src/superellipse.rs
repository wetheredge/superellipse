#[cfg(not(feature = "const_generics"))]
#[derive(Debug, Copy, Clone)]
pub struct Superellipse {
    n: f64,
    a: f64,
    b: f64,
}

#[cfg(not(feature = "const_generics"))]
impl Superellipse {
    pub fn new(n: f64, a: f64, b: f64) -> Self {
        Self { n, a, b }
    }

    pub fn new_squircle(size: f64) -> Self {
        Self {
            n: 4.,
            a: size,
            b: size,
        }
    }

    pub fn get_y(&self, x: f64) -> f64 {
        self.b * (1. - (x / self.a).powf(self.n)).powf(1. / self.n)
    }

    pub fn get_x(&self, y: f64) -> f64 {
        self.a * (1. - (y / self.b).powf(self.n)).powf(1. / self.n)
    }
}

#![cfg_attr(feature = "const_generics", feature(const_generics))]

#[cfg_attr(feature = "const_generics", path = "const_generics.rs")]
mod superellipse;
pub use crate::superellipse::*;

#[cfg(test)]
mod tests {
    use super::*;

    #[cfg(feature = "const_generics")]
    fn get_superellipse() -> Superellipse<2., 1., 0.5> {
        Superellipse::new()
    }

    #[cfg(not(feature = "const_generics"))]
    fn get_superellipse() -> Superellipse {
        Superellipse::new(2., 1., 0.5)
    }

    #[test]
    fn superellipse() {
        let superellipse = get_superellipse();

        assert_eq!(superellipse.get_y(0.), 0.5);
        assert_eq!(superellipse.get_y(0.1), 0.49749371855331);
        assert_eq!(superellipse.get_y(0.2), 0.4898979485566356);
        assert_eq!(superellipse.get_y(0.3), 0.47696960070847283);
        assert_eq!(superellipse.get_y(0.4), 0.458257569495584);
        assert_eq!(superellipse.get_y(0.5), 0.4330127018922193);
        assert_eq!(superellipse.get_y(0.6), 0.4);
        assert_eq!(superellipse.get_y(0.7), 0.3570714214271425);
        assert_eq!(superellipse.get_y(0.8), 0.29999999999999993);
        assert_eq!(superellipse.get_y(0.9), 0.21794494717703364);
        assert_eq!(superellipse.get_y(1.), 0.);
    }
}

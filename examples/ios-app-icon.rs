use image::{Rgba, RgbaImage};
use superellipse::Squircle;

const SIZE: u32 = 180;
const RADIUS: u32 = SIZE / 2;
const SQUIRCLE_RADIUS: f64 = (RADIUS - 1) as f64;

fn get_color(alpha: u8) -> Rgba<u8> {
    Rgba([0, 0, 0, alpha])
}

fn antialias_range(x: f64) -> std::ops::RangeInclusive<u8> {
    let floor = x.floor();
    let ceil = x.ceil();

    floor as u8..=(ceil + 1.) as u8
}

fn put_pixel_symmetrically(img: &mut RgbaImage, x: u32, y: u32, color: Rgba<u8>) {
    // All 4 quadrants
    for x in [RADIUS + x, RADIUS - 1 - x] {
        for y in [RADIUS + y, RADIUS - 1 - y] {
            // Each quadrant is also symmetrical across y=x
            img.put_pixel(x, y, color);
            img.put_pixel(y, x, color);
        }
    }
}

fn main() {
    let squircle = Squircle::<SQUIRCLE_RADIUS>::new();

    let mut img = RgbaImage::new(SIZE, SIZE);

    for x in 0..RADIUS {
        let real_y = squircle.get_y(x as f64);

        // Reached the line of symmetry y=x
        if (real_y + 1.) < x as f64 {
            break;
        }

        for y in antialias_range(real_y) {
            let percent = 1. - (real_y - y as f64).abs();

            if !(0.0..=1.0).contains(&percent) || y as u32 >= RADIUS {
                continue;
            }

            let alpha = (255. * percent).round() as u8;
            let color = if y as f64 > real_y {
                get_color(alpha)
            } else {
                get_color(255)
            };

            put_pixel_symmetrically(&mut img, x, y as u32, color);
        }

        // We only need to cover above the line y=x because of `put_pixel_symmetrically`.
        for y in x..real_y.round() as u32 {
            put_pixel_symmetrically(&mut img, x, y, get_color(255));
        }
    }

    img.save("ios-app-icon.png")
        .expect("failed to write image to file");
}
